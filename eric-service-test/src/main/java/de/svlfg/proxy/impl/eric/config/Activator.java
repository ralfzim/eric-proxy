package de.svlfg.proxy.impl.eric.config;

import java.util.Dictionary;
import java.util.Hashtable;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import de.svlfg.proxy.eric.config.EricProperties;
import de.svlfg.proxy.eric.config.FileEricProperties;

public class Activator implements BundleActivator{

	@Override
	public void start(BundleContext context) throws Exception {
		FileEricProperties ericProperties = new FileEricProperties(Activator.class, "eric.properties");
		Dictionary<String, String> properties =  new Hashtable<>();
		properties.put("priority", "500");
		context.registerService(EricProperties.class, ericProperties, properties);
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		
	}

}
