package de.svlfg.impl.eric.library;

/**
 * Exception-Klasse für Fehlermeldungen, bei denen eine Serverantwort vorhanden
 * ist.
 */
public class ResponseException extends EricException {

	private static final long serialVersionUID = 1766161277337242446L;

	private final String serverRepsonse;

	public ResponseException(final int rc, final String errorMessage, final String serverResponse) {
		super(rc, errorMessage);

		this.serverRepsonse = serverResponse;
	}

	public String getServerRepsonse() {
		return serverRepsonse;
	}

	public String toString() {
		return "Serverantwort:\n" + serverRepsonse;
	}
}
