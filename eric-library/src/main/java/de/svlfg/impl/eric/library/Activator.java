package de.svlfg.impl.eric.library;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.AbstractMap.SimpleEntry;
import java.util.Collection;
import java.util.Optional;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

import de.svlfg.eric.library.EricapiLibrary;
import de.svlfg.proxy.eric.config.EricProperties;

public class Activator implements BundleActivator {

	@Override
	public void start(BundleContext context) throws Exception {
		Optional<EricProperties> ericProperties = loadEricProperties(context);
		ericProperties.orElseThrow(() -> new RuntimeException("EricProperties: reference is missing"));
		registerEricapiLibrary(ericProperties, context);

	}

	private Optional<EricProperties> loadEricProperties(BundleContext context) throws InvalidSyntaxException {
		Collection<ServiceReference<EricProperties>> ericPropertiesReference = context
				.getServiceReferences(EricProperties.class, null);
		Optional<EricProperties> ericProperties = ericPropertiesReference.stream()//
				.map(reference -> new SimpleEntry<Object, ServiceReference<EricProperties>>(
						context.getProperty("priority"), reference)) //
				.map(e -> new SimpleEntry<String, ServiceReference<EricProperties>>(
						e.getKey() == null ? "100" : e.getKey().toString(), e.getValue())) //
				.sorted((e1, e2) -> e1.getKey().compareTo(e2.getKey())) //
				.map(e -> context.getService(e.getValue())).findFirst();
		return ericProperties;
	}

	private void registerEricapiLibrary(Optional<EricProperties> ericProperties, BundleContext context)
			throws URISyntaxException, EricException, WrapperException {
		URI uri = Activator.class.getClassLoader().getResource("root.properties").toURI();
		Path path = Paths.get(uri).getParent().resolve("linux-x86-64");
		String ericPath = path.toString();
		String logPath = path.getParent().toString();
		EricFactory.initialize(ericPath, logPath);
		ericProperties.get().getCommunicationProperties().stream()//
				.forEach(e -> EricFactory.getInstance().EricEinstellungSetzen(e.getKey(), e.getValue()));
		context.registerService(EricapiLibrary.class, EricFactory.getInstance(), null);
	}

	@Override
	public void stop(BundleContext context) throws Exception {

	}

}
