FROM frekele/gradle:latest as builder


RUN  mkdir -p /tmp/ 

COPY eric-proxy        /tmp/eric-proxy
COPY eric-top          /tmp/eric-top
COPY eric-library      /tmp/eric-library
COPY eric-service      /tmp/eric-service
COPY eric-service-test /tmp/eric-service-test
COPY eric-service-prod /tmp/eric-service-prod




RUN  cd /tmp/eric-top  && \
     gradle assemble


FROM openjdk:8-jre  as virgo-common

ENV VIRGO_VERSION 3.7.2
ENV VIRGO virgo-tomcat-server-$VIRGO_VERSION
ENV VIRGO_HOME /opt/virgo


#  RUN env
#  RUN apk add --update curl libarchive-tools bash


RUN \
    wget -O virgo.zip http://nexus-pi.rz.lsv.de:8081/repository/public/eclipse/org/virgo-tomcat-server/$VIRGO_VERSION/$VIRGO.zip && \
    mkdir -p /opt && \
    unzip -d /opt/ virgo.zip >/dev/null && \
    ln -s /opt/${VIRGO}.RELEASE $VIRGO_HOME && \
    cp $VIRGO_HOME/configuration/java-server.profile . && \
    cat java-server.profile | sed '/ osgi.ee*/ s/1\.7/1.7, 1.8/' >$VIRGO_HOME/configuration/java-server.profile && \
    cp $VIRGO_HOME/configuration/tomcat-server.xml . && \
    cat tomcat-server.xml | sed '/Connector/ s/127\.0\.0\.1/0.0.0.0/' >$VIRGO_HOME/configuration/tomcat-server.xml && \
    cp $VIRGO_HOME/configuration/org.eclipse.virgo.kernel.users.properties . && \
    cat org.eclipse.virgo.kernel.users.properties | sed '/user.admin/ s/=admin/=A23pzWe)"8cV/' >$VIRGO_HOME/configuration/org.eclipse.virgo.kernel.users.properties && \
	echo "user.tomcat=PWD4ERIC" >>$VIRGO_HOME/configuration/org.eclipse.virgo.kernel.users.properties && \ 
	echo "role.tomcat=admin" >>$VIRGO_HOME/configuration/org.eclipse.virgo.kernel.users.properties && \ 
    rm java-server.profile tomcat-server.xml org.eclipse.virgo.kernel.users.properties virgo.zip && \
    mkdir -p /tmp/eric

COPY --from=builder /tmp/eric-proxy/build/libs/         /tmp/eric/    
COPY --from=builder /tmp/eric-library/build/libs/       /tmp/eric/    
COPY --from=builder /tmp/eric-service/build/libs/       /tmp/eric/    
COPY --from=builder /tmp/eric-service-test/build/libs/  /tmp/eric/    
COPY --from=builder /tmp/eric-service-prod/build/libs/  /tmp/eric/    

COPY   eric-top/plan/eric-application.xml                /opt/virgo/

RUN \
    wget http://nexus-pi.rz.lsv.de:8081/repository/public/net/java/dev/jna/jna/4.5.1/jna-4.5.1.jar && \
    mv jna*.jar /tmp/eric/ && \
    mkdir -p $VIRGO_HOME/repository/usr && \
    vmc(){ mv /tmp/eric/$1 $VIRGO_HOME/repository/usr/z$2_$1; } && \
    vmc jna-4.5.1.jar 00 && \
    vmc eric-service-0.1.0.jar 10 && \
    vmc eric-service-test-0.1.0.jar 20 && \
    vmc eric-service-prod-0.1.0.jar 20 && \
    vmc eric-library-0.1.1.jar 30 && \
    vmc eric-proxy-0.0.1.jar 40 && \
    rm -rf /tmp/eric

    
RUN \    
	adduser --shell /bin/bash --home $VIRGO_HOME virgo && \
	chmod u+x $VIRGO_HOME/bin/*.sh && \
	chown virgo:virgo /opt/${VIRGO}.RELEASE -R && \
	echo ready
	

USER virgo

WORKDIR $VIRGO_HOME
EXPOSE  8080

CMD ["bin/startup.sh"]

FROM virgo-common

ARG ETYP=TEST
ENV ETYP="${ETYP}" 

RUN \
    if [ "${ETYP}" = "PROD" ]; then rm $VIRGO_HOME/repository/usr/*eric-service-test*.jar ; else rm $VIRGO_HOME/repository/usr/*eric-service-prod*.jar; fi  && \
    SED_DEL="service-prod" && \
    if [ "${ETYP}" = "PROD" ]; then SED_DEL="service-test"; fi && \
    sed "/$SED_DEL/ d"  eric-application.xml > pickup/eric-application.plan

