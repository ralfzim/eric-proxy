package de.elster.eric.wrapper;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.InputStream;
import java.net.URL;
import java.util.Iterator;
import java.util.Optional;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.AttachmentPart;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPMessage;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class LoopTest {

//	private final static String SERVER = "http://lin0017.rz.lsv.de:8080";
	private final static String SERVER = TestConfiguration.SERVER;

	@Test
	public void testElsterDatenteil01() throws Exception {
		SOAPMessage resp = send(SERVER, "1/ElsterLohn/Lohnersatzleistung/220000000", "ElsterDatenteil.txt",Optional.empty());
		resp.writeTo(System.out);
		assertNotNull(resp.getSOAPBody());
	}
	@Test
	public void testElsterDatenteil02() throws Exception {
		SOAPMessage resp = send(SERVER, "1/ElsterLohn/Lohnersatzleistung/220000000", "ElsterDatenteil.txt",Optional.of(2222));
		resp.writeTo(System.out);
		assertNotNull(resp.getSOAPBody());
	}

	@Test
	public void testElsterDatenteil03() throws Exception {
		SOAPMessage resp = send(SERVER, "2/ElsterDatenabholung/ElsterLohnDaten/700000004", "ElsterDatenteil02.txt",Optional.of(0));
		resp.writeTo(System.out);
		resp.getSOAPBody();
		Iterator atts = resp.getAttachments();
		assertTrue(atts.hasNext());
		atts.next();
		assertFalse(atts.hasNext());
	}
	@Test
	public void testElsterDatenteil04() throws Exception {
//		HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
//		new EricProxyException(2234234, "error").write(response);
		SOAPMessage resp = send(SERVER, "error/ElsterLohn/Lohnersatzleistung/220000000", "ElsterDatenteil.txt",Optional.empty());
		resp.writeTo(System.out);
		assertNotNull(resp.getSOAPBody());
	}


	private SOAPMessage send(String server2, String string, String string2,Optional<Integer> transferHandle) throws Exception {
		InputStream is = LoopTest.class.getClassLoader().getResourceAsStream(string2);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		Document doc = factory.newDocumentBuilder().parse(is);
		SOAPMessage message = MessageFactory.newInstance().createMessage();

		SOAPBody soapBody = message.getSOAPBody();
		Node node = ((Element) soapBody).getOwnerDocument().adoptNode(doc.getDocumentElement());
		soapBody.appendChild(node);
		if (transferHandle.isPresent()) {
			AttachmentPart part = message.createAttachmentPart();
			byte[] b = Integer.toString(transferHandle.get()).getBytes();
			part.setRawContentBytes(b, 0, b.length, "text/plain");
			message.addAttachmentPart(part);
		}
		message.saveChanges();
		SOAPConnection connection = SOAPConnectionFactory.newInstance().createConnection();
		URL url = new URL(server2 + "/eric-proxy/loop/" + string);
		SOAPMessage result = connection.call(message, url);
		return result;
	}
}
