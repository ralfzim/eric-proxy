package de.svlfg.proxy.eric;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;

import de.svlfg.proxy.eric.config.UserConfig;

public class EricAbstractRequest {

	private UserConfig proxyConfig;
	private String requestBody;
	private byte[] pathToKeystore;

	public EricAbstractRequest(UserConfig proxyConfig) {
		this.proxyConfig = proxyConfig;
	}

	public void setBody(String string) {
		this.requestBody = string;
	
	}

	public String getRequestBody() {
		return requestBody;
	}

	public UserConfig getUserConfig() {
		return proxyConfig;
	}

	public byte[] getPathToKeystore() throws IOException {
		if (this.pathToKeystore == null) {
			Path path = Files.createTempFile("keystore", "pxf");
			Files.copy(new ByteArrayInputStream(getUserConfig().getCertificate()), path,
					StandardCopyOption.REPLACE_EXISTING);
			String string = path.toFile().toString();
			byte[] bytes = string.getBytes();
			this.pathToKeystore = Arrays.copyOf(bytes, bytes.length + 1);
		}
		return pathToKeystore;
	}

}
