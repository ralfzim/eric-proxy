package de.svlfg.proxy.eric;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.AttachmentPart;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class EricProxyResultSuccess implements EricProxyResult {
	private final static Logger log = LoggerFactory.getLogger(EricProxyResultSuccess.class);

	private Document document;
	private Optional<Integer> transferHandler = Optional.empty();

	@Override
	public void write(HttpServletResponse resp) throws ServletException {
		try {
			SOAPMessage message = MessageFactory.newInstance().createMessage();
			Node node = ((Element) message.getSOAPBody()).getOwnerDocument().adoptNode(document.getDocumentElement());
			message.getSOAPBody().appendChild(node);
			if (transferHandler.isPresent()) {
				AttachmentPart part = message.createAttachmentPart();
				part.setContentType("text/ascii");
				byte[] content = Integer.toString(transferHandler.get()).getBytes("iso-8859-15");
				part.setRawContentBytes(content, 0, content.length, "text/ascii");
				message.addAttachmentPart(part);
				message.saveChanges();
				resp.setContentType(message.getMimeHeaders().getHeader("content-type")[0]);
			} else {
				resp.setContentType("text/xml");
			}
			message.writeTo(resp.getOutputStream());
		} catch (SOAPException | IOException e) {

		}
	}

	public void setBody(byte[] bytes) throws IOException {
		try {
			DocumentBuilderFactory newInstance = DocumentBuilderFactory.newInstance();
			newInstance.setNamespaceAware(true);
			document = newInstance.newDocumentBuilder().parse(new ByteArrayInputStream(bytes));
		} catch (SAXException | IOException | ParserConfigurationException e) {
			throw new IOException(e);
		}

	}

	public void setTransferHandle(Optional<Integer> of) {
		log.debug("setTransferHandle: {}",of);
		this.transferHandler = Objects.requireNonNull(of,"The transfer handle must not be null!");
	}
}
