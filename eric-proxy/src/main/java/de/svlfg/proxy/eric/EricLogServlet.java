package de.svlfg.proxy.eric;

import java.io.IOException;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.svlfg.proxy.eric.la.LogAccess;
import de.svlfg.proxy.eric.la.LogResponse;

public class EricLogServlet extends HttpServlet {
	private final static Logger log = LoggerFactory.getLogger(EricLogServlet.class);


	@Override
	protected void doGet(HttpServletRequest servletRequest, HttpServletResponse resp) throws ServletException, IOException {
		String pattern = Optional.ofNullable(servletRequest.getParameter("pattern")).orElseGet(()-> "^"+LocalDate.now().toString()+".*");
		log.debug("doGet {}",pattern);
		LogAccess logAccess = new LogAccess(Paths.get("./"), Paths.get("eric.log"), pattern);
		new LogResponse(logAccess, resp.getWriter()).write();
	}




	@Override
	public void init() throws ServletException {
		super.init();
	}


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
