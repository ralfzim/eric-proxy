package de.svlfg.proxy.eric;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;

import de.svlfg.eric.library.EricapiLibrary;
import de.svlfg.eric.library.EricapiLibrary.eric_verschluesselungs_parameter_t;
import de.svlfg.proxy.eric.config.UserConfig;

public class EricProxyRequest extends EricAbstractRequest {

	private final static Logger log = LoggerFactory.getLogger(EricProxyRequest.class);
	private final static Pattern PATH4 = Pattern.compile("^/([^/]+)/([^/]+)/([^/]+)/([^/]+)$");
	private final static Pattern PATH3 = Pattern.compile("^/([^/]+)/([^/]+)/([^/]+)$");
	private final static StringEncoding STRCODE = new StringEncoding();
	private String datenart;
	private String datenartVersion;
	private EricapiLibrary ericapiLib;
	private String testmerker;
	private Optional<Integer> transferHandle = Optional.empty();
	private String verfahren;

	public EricProxyRequest(UserConfig proxyConfig, EricapiLibrary ericapiLib) {
		super(proxyConfig);
		this.ericapiLib = ericapiLib;
	}

	private String getDatenart() {
		return datenart;
	}

	private String getDatenartVersion() {
		return this.datenartVersion;
	}

	private String getTestmerker() {
		return testmerker;
	}

	private Optional<Integer> getTransferHandle() {
		return transferHandle;
	}

	private String getVerfahren() {
		return verfahren;
	}

	public void readVerfahrenDatenartTestmerker(String pathInfo) {
		Matcher m = null;
		if ((m = PATH4.matcher(pathInfo)).find()) {
			this.verfahren = m.group(1);
			this.datenart = m.group(2);
			this.datenartVersion = m.group(3);
			this.testmerker = m.group(4);
		} else if ((m = PATH3.matcher(pathInfo)).find()) {
			this.verfahren = m.group(1);
			this.datenart = m.group(2);
			this.datenartVersion = m.group(3);
			this.testmerker = null;
		} else {
			throw new IllegalArgumentException(
					"EricProxyRequest can't determine Verfahren, Datenart and Testmerker: " + pathInfo);
		}
		try {
			if (Objects.nonNull(this.testmerker) && Integer.parseInt(this.testmerker.trim()) == 0) {
				this.testmerker = null;
			}
		} catch (NumberFormatException e) {
		}
	}

	public EricProxyResult sendRequest() throws IOException, EricProxyException {
		eric_verschluesselungs_parameter_t cryptoParams = createCryptoParameters();

		// 2. Mit EricGetPublicKey() den öffentlichen Schlüssel des Zertifikats auslesen

		String publicKey = getPublicKey(cryptoParams);

		String xmlData = createTechnicalHeader(publicKey);
		xmlData = STRCODE.decode(xmlData);
		// 7. Den Versand mit EricBearbeiteVorgang() durchführen

		cryptoParams = createCryptoParameters();
		EricProxyResultSuccess result = sendEricData(cryptoParams, xmlData);
		return result;

	}

	private EricProxyResultSuccess sendEricData(eric_verschluesselungs_parameter_t cryptoParams, String xmlData)
			throws EricProxyException, UnsupportedEncodingException, IOException {
		int rc;
		PointerByReference ericResponseBuffer = ericapiLib.EricRueckgabepufferErzeugen();
		PointerByReference serverResponseBuffer = ericapiLib.EricRueckgabepufferErzeugen();
		final IntByReference transferHandle = this.getTransferHandle().map(e -> new IntByReference(e)).orElse(null);

		log.debug("vor EricBearbeiteVorgang  datenart {} transferHandle {} message {}", this.getDatenart(),
				transferHandle, xmlData);
		rc = ericapiLib.EricBearbeiteVorgang(xmlData, //
				this.getDatenartVersion(), //
				EricapiLibrary.ERIC_SENDE, //
				null, //
				cryptoParams, //
				transferHandle, //
				ericResponseBuffer,//
				serverResponseBuffer);

		log.debug("nach EricBearbeiteVorgang returnCode {}", rc);
		if (rc != 0) {
			String errorMessage = ericapiLib.EricRueckgabepufferInhalt(ericResponseBuffer);
			log.error("nach EricBearbeiteVorgang Fehler {} message {}", rc, errorMessage);
			throw new EricProxyException(rc, errorMessage);
		}
		EricProxyResultSuccess result = new EricProxyResultSuccess();
		String ericRueckgabepufferInhalt = ericapiLib.EricRueckgabepufferInhalt(serverResponseBuffer);
		ericRueckgabepufferInhalt = STRCODE.decode(ericRueckgabepufferInhalt);
		log.debug("nach EricBearbeiteVorgang Erfolg content ohne  {}", ericRueckgabepufferInhalt);
		result.setBody(ericRueckgabepufferInhalt.getBytes("utf-8"));
		log.debug("nach EricBearbeiteVorgang Erfolg content {}", ericRueckgabepufferInhalt);
		this.getTransferHandle().ifPresent(e -> {
			int value = transferHandle.getValue();
			result.setTransferHandle(Optional.of(value));
			log.debug("nach EricBearbeiteVorgang Erfolg transferHandle {}", value);

		});
		return result;
	}

	private String createTechnicalHeader(String publicKey) {
		int rc;
		PointerByReference returnBuffer = ericapiLib.EricRueckgabepufferErzeugen();
		log.debug("vor EricCreateTH verfahren {} datenart {} testmerker {} senderName {} daten\n{}", this.getVerfahren(),
				this.getDatenart(), this.getTestmerker(), this.getUserConfig().getSenderName(), this.getRequestBody());

		rc = ericapiLib.EricCreateTH(//
				this.getRequestBody(), //
				this.getVerfahren(), //
				this.getDatenart(), //
				"send-Auth",
				this.getTestmerker(), //
				this.getUserConfig().getSenderId(), //
				this.getUserConfig().getSenderName(), //
				null, //
				publicKey, //
				returnBuffer);
		if (rc != 0) {
			log.error("nach EricCreateTH rc={}", rc);
			throw new RuntimeException("Error creating TransferHeader! " + rc);
		}

		String xmlData = ericapiLib.EricRueckgabepufferInhalt(returnBuffer);
		return xmlData;
	}

	private String getPublicKey(eric_verschluesselungs_parameter_t cryptoParams) {
		int rc;
		PointerByReference rueckgabePuffer = ericapiLib.EricRueckgabepufferErzeugen();
		rc = ericapiLib.EricGetPublicKey(cryptoParams, rueckgabePuffer);
		if (rc != 0) {
			log.error("nach EricGetPublicKey rc={}", rc);
			throw new RuntimeException("Error loading public key! " + rc);
		}
		String publicKey = ericapiLib.EricRueckgabepufferInhalt(rueckgabePuffer);
		return publicKey;
	}

	private eric_verschluesselungs_parameter_t createCryptoParameters() throws IOException {
		return Activator.getCertificateProvider().createCryptoParameters();
	}

	public void setTransferHandle(Optional<Integer> of) {
		this.transferHandle = of;
	}

}
