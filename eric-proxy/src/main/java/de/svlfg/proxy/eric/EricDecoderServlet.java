package de.svlfg.proxy.eric;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

public class EricDecoderServlet extends HttpServlet {
	private final static Logger log = LoggerFactory.getLogger(EricDecoderServlet.class);


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		try {
			XmlUtils xmlUtils = XmlUtils.getInstance();
			EricDecoderRequest decoderRequest = new EricDecoderRequest(Activator.getProxyConfig(),Eric.getLibrary());
			SOAPMessage soapMessage = xmlUtils.readSoapMessage(req);
			decoderRequest.setBody(xmlUtils.toString(xmlUtils.selectFirstBodyElement(soapMessage)));
			log.debug("Body_Element {}",decoderRequest.getRequestBody());
			decoderRequest.setDecoderContent(selectDatenpaket(decoderRequest));
			EricProxyResult result = decoderRequest.decode();
			result.write(resp);
		} catch (SOAPException | JAXBException  | NullPointerException | IOException | SAXException | ParserConfigurationException | XPathExpressionException e) {
			log.error("Error when calling decoder", e);
			new EricProxyException(-1, e.getMessage()).write(resp);
		}
	}

	private String selectDatenpaket(EricDecoderRequest decoderRequest) throws JAXBException, SAXException, IOException, ParserConfigurationException, XPathExpressionException {
		log.debug("selectDatenpaket: {}",decoderRequest.getRequestBody());
		ByteArrayInputStream bis = new ByteArrayInputStream(decoderRequest.getRequestBody().getBytes("iso-8859-15"));
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(false);
		Document document = factory.newDocumentBuilder().parse(bis);
		XPath xpath = XPathFactory.newInstance().newXPath();
		NodeList e =  (NodeList) xpath.evaluate("//Datenpaket", document.getDocumentElement(), XPathConstants.NODESET);
		if (e != null && e.getLength() > 0) {
			log.debug("selectDatenpaket found Element: {}",e);
			NodeList nl = e.item(0).getChildNodes();
			StringBuilder b = new StringBuilder();
			for (int i = 0; i < nl.getLength(); i++) {
				Node n = nl.item(i);
				if (n instanceof Text) {
					b.append((((Text)n).getData()));
				}
			}
			log.debug("selectDatenpaket out: {}",b.toString());
			return b.toString();
		} else {
			throw new IOException();
		}
	}

	@Override
	public void init() throws ServletException {
		super.init();
	}



}
