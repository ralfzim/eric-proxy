package de.svlfg.proxy.eric;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.jna.ptr.IntByReference;

import de.svlfg.eric.library.EricapiLibrary;
import de.svlfg.eric.library.EricapiLibrary.eric_verschluesselungs_parameter_t;
import de.svlfg.proxy.eric.config.UserConfig;

public class EricCertificateProvider {
	private final static Logger log = LoggerFactory.getLogger(EricCertificateProvider.class);
	private byte[] pathToKeystore;
	private UserConfig proxyConfig;
	private EricapiLibrary ericapiLib;


	public EricCertificateProvider(UserConfig proxyConfig, EricapiLibrary library) {
		this.proxyConfig = proxyConfig;
		this.ericapiLib = library;
		try {
			init();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

	}
	private IntByReference certificateHandle = new IntByReference();
	private void init() throws IOException {
		IntByReference pinSupport = new IntByReference();

		log.debug("vor EricGetHandleToCertificate keystore={}, pin={}", new String(this.getPathToKeystore()),
				this.getUserConfig().getCertificatePin());
		int rc = ericapiLib.EricGetHandleToCertificate(certificateHandle, pinSupport, this.getPathToKeystore());
		if (rc != 0) {
			log.error("nach EricGetHandleToCertificate rc={}", rc);
			throw new RuntimeException("Error in KeyStore Initialisation! " + rc);
		}
	}
	private byte[] getPathToKeystore() throws IOException {
		if (this.pathToKeystore == null) {
			Path path = Files.createTempFile("keystore", "pxf");
			Files.copy(new ByteArrayInputStream(getUserConfig().getCertificate()), path,
					StandardCopyOption.REPLACE_EXISTING);
			String string = path.toFile().toString();
			byte[] bytes = string.getBytes();
			this.pathToKeystore = Arrays.copyOf(bytes, bytes.length + 1);
		}
		return pathToKeystore;
	}

	public void release() {
		int rc = ericapiLib.EricCloseHandleToCertificate(certificateHandle.getValue());
		if (rc != 0) {
			log.error("nach EricCloseHandleToCertificate rc={}", rc);
			throw new RuntimeException("Error in KeyStore releasing! " + rc);
		}
		this.certificateHandle = null;
	}

	private UserConfig getUserConfig() {
		return proxyConfig;
	}
	
	public eric_verschluesselungs_parameter_t createCryptoParameters() throws IOException {
		eric_verschluesselungs_parameter_t cryptoParams = new eric_verschluesselungs_parameter_t();
		cryptoParams.version = 2;
		cryptoParams.zertifikatHandle = certificateHandle.getValue();
		cryptoParams.pin = this.getUserConfig().getCertificatePin();
		cryptoParams.abrufCode = null;
		return cryptoParams;
	}
	public IntByReference getCertificateHandle() {
		return this.certificateHandle;
	}

}
