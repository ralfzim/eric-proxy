package de.svlfg.proxy.eric.la;

import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Optional;
import java.util.function.BiPredicate;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LogAccess {

	private Path logFile;
	private Predicate<String> includePattern;
	private Path startDir;
	private String pattern;

	public LogAccess(Path startDir, Path logFile, String pattern) {
		this.logFile = logFile;
		this.startDir = startDir;
		this.includePattern = Pattern.compile(pattern).asPredicate();
		this.pattern = pattern;
	}

	public String read() throws IOException {
		return findLogFile() //
				.flatMap(lf -> Optional.of(lines(lf).filter(includePattern).collect(Collectors.joining("\r\n"))))
				.orElse("");
	}

	private Stream<String> lines(Path lf) {
		try {
			return Files.lines(lf);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private Optional<Path> findLogFile() throws IOException {
		BiPredicate<Path, BasicFileAttributes> filePattern = (p, attr) -> {
			return p.getFileName().equals(logFile);
		};
		return Files.find(this.startDir, Integer.MAX_VALUE, filePattern, FileVisitOption.FOLLOW_LINKS).findFirst();
	}

	public String getPattern() {
		return pattern;
	}

}
